const colors = [
  "gray",
  "red",
  "blue",
  "pink",
  "violet",
  "green",
  "orange",
  "brown",
];
const renderColor = () => {
  let contentHTML = "";
  for (let index = 0; index < colors.length; index++) {
    const selectColor = colors[index];
    contentHTML += `<button class ="btn" style = "background-color = "${selectColor}" onclick = "changeColor('${selectColor}')" >${selectColor}</button>`;
  }
  document.getElementById("buttonList").innerHTML = contentHTML;
};
renderColor();
const changeColor = (selectColor) => {
  document.getElementById("house").style.color = selectColor;
};
