const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermil lion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

const renderColor = () => {
  let contentHTML = "";
  for (let index = 0; index < colorList.length; index++) {
    const color = colorList[index];
    contentHTML += `<button class ="btn color-button ${color}"  onclick = "changeColor('${color}')" ></button>`;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
};
renderColor();
const changeColor = (color) => {
  document.getElementById("house").style.color = color;
};
