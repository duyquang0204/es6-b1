let tinhDiem = function (
  _diemToan,
  _diemLy,
  _diemHoa,
  _diemVan,
  _diemSu,
  _diemDia,
  _diemEnglish
) {
  this.diemToan = _diemToan;
  this.diemLy = _diemLy;
  this.diemHoa = _diemHoa;
  this.diemVan = _diemVan;
  this.diemSu = _diemSu;
  this.diemDia = _diemDia;
  this.diemEnglish = _diemEnglish;
};

let diemMonHoc = () => {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemEnglish = document.getElementById("inpEnglish").value * 1;
  // console.log({ diemToan, diemLy });
  let diem = new tinhDiem(
    diemToan,
    diemLy,
    diemHoa,
    diemVan,
    diemSu,
    diemDia,
    diemEnglish
  );
  return diem;
};

let diemTrungBinh = (...scores) => {
  let total = 0;
  let scoreAverage = 0;
  for (let i = 0; i < scores.length; i++) {
    total += scores[i];
    scoreAverage = total / scores.length;
  }
  return scoreAverage;
};

console.log(diemTrungBinh(2, 3, 4, 5));

let tinhDiemKhoi1 = () => {
  let diem = diemMonHoc();
  console.log(diem.diemToan);
  let diemTB1 = diemTrungBinh(diem.diemToan, diem.diemLy, diem.diemHoa);
  console.log("diemTrungBinh: ", diemTB1);

  document.getElementById("tbKhoi1").innerHTML = diemTB1;
};

let tinhDiemKhoi2 = () => {
  let diem = diemMonHoc();

  let diemTB2 = diemTrungBinh(
    diem.diemVan,
    diem.diemSu,
    diem.diemDia,
    diem.diemEnglish
  );

  console.log("diemTrungBinh: ", diemTB2);
  document.getElementById("tbKhoi2").innerHTML = diemTB2;
};
